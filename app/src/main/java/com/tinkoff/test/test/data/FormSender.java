package com.tinkoff.test.test.data;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tinkoff.test.test.R;

/**
 * @author Sergei Solodkov
 */

public class FormSender {

    @NonNull
    private final Context context;

    public FormSender(@NonNull Context context) {
        this.context = context;
    }

    public void sendForm(@NonNull Form form, @NonNull final FormSenderListener listener){
        FirebaseDatabase.getInstance().getReference().child(buildKey(form)).setValue(form, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null){
                    listener.onFormSent();
                } else {
                    listener.onError(databaseError.getMessage());
                }
            }
        });
    }

    private String buildKey(@NonNull Form form){
        return context.getString(R.string.db_key_format, form.firstName, form.lastName, form.email).replaceAll("[.#$\\[\\]]","");
    }

    public interface FormSenderListener {

        void onFormSent();

        void onError(@NonNull String message);

    }

}
