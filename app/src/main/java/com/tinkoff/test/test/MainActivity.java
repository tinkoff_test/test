package com.tinkoff.test.test;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.tinkoff.test.test.data.FormSender;
import com.tinkoff.test.test.data.QuestionsProvider;

import static com.tinkoff.test.test.MainContract.*;

public class MainActivity extends AppCompatActivity implements MainView {

    private static final String QUESTION_NUMBER = "QUESTION_NUMBER";

    private MainPresenter presenter;
    private View rootView;
    private View questionGroup;
    private View personalDataGroup;
    private View successNotification;
    private View progressBar;
    private TextView questionView;
    private TextView questionsProgress;
    private EditText answer;
    private EditText firstName;
    private EditText lastName;
    private EditText email;
    private int currentQuestion = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rootView = findViewById(R.id.main_frame);
        progressBar = findViewById(R.id.progress_bar);
        questionGroup = findViewById(R.id.question_group);
        personalDataGroup = findViewById(R.id.personal_data_group);
        successNotification = findViewById(R.id.success_notification);
        questionView = (TextView) findViewById(R.id.question_text);
        answer = (EditText) findViewById(R.id.answer);
        firstName = (EditText) findViewById(R.id.input_firstname);
        lastName = (EditText) findViewById(R.id.input_lastname);
        email = (EditText) findViewById(R.id.input_email);
        questionsProgress = (TextView) findViewById(R.id.questions_progress);
        if (savedInstanceState != null){
            currentQuestion = savedInstanceState.getInt(QUESTION_NUMBER, -1);
        }
        presenter = new MainActivityPresenter(new FormSender(getApplicationContext()), new QuestionsProvider());
        presenter.attach(this, currentQuestion);
        findViewById(R.id.button_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.processAnswer(answer.getText().toString());
                answer.setText("");
            }
        });
        findViewById(R.id.button_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });
        email.setOnKeyListener(new View.OnKeyListener(){
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event){
                if(keyCode == KeyEvent.KEYCODE_ENTER){
                    submit();
                }
                return false;
            }
        });
    }

    @Override
    public void showQuestion(@NonNull String question, int number, int count) {
        currentQuestion = number;
        questionView.setText(question);
        showKeyboard(answer);
        questionsProgress.setText(getString(R.string.questions_progress, number + 1, count));
    }

    @Override
    public void showPersonalDataForm() {
        questionGroup.setVisibility(View.GONE);
        personalDataGroup.setVisibility(View.VISIBLE);
        successNotification.setVisibility(View.GONE);
        firstName.requestFocus();
        showKeyboard(firstName);
    }

    @Override
    public void showMessage(@NonNull String message){
        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(int messageResId) {
        showMessage(getString(messageResId));
    }

    @Override
    public void showSuccessNotification() {
        questionGroup.setVisibility(View.GONE);
        personalDataGroup.setVisibility(View.GONE);
        successNotification.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoading(boolean isShown) {
        progressBar.setVisibility(isShown ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(QUESTION_NUMBER, currentQuestion);
    }

    @Override
    protected void onDestroy() {
        presenter.release();
        super.onDestroy();
    }

    private void submit(){
        hideKeyboard();
        presenter.processSubmit(firstName.getText().toString(),
                lastName.getText().toString(),
                email.getText().toString());
    }

    private void showKeyboard(@NonNull View input){
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(input, InputMethodManager.SHOW_IMPLICIT);
    }

    private void hideKeyboard(){
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
    }

}
