package com.tinkoff.test.test.data;

import java.util.Map;

/**
 * @author Sergei Solodkov
 */
public class Form {

    public Map<String, String> answers;
    public String firstName;
    public String lastName;
    public String email;

    public Form(){}

    public Form(Map<String, String> answers, String firstName, String lastName, String email) {
        this.answers = answers;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }
}
