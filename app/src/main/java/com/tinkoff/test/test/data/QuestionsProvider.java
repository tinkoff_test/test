package com.tinkoff.test.test.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import java.util.List;

/**
 * @author Sergei Solodkov
 */

public class QuestionsProvider {

    @NonNull
    private final FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
    @NonNull
    private final QuestionsParser questionsParser = new QuestionsParser();

    public void requestQuestions(@NonNull final QuestionsListener questionsListener){
        firebaseRemoteConfig.fetch()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            firebaseRemoteConfig.activateFetched();
                            List<String> questions = questionsParser.parseQuestions(firebaseRemoteConfig.getString("questions"));
                            questionsListener.onQuestionsReceived(questions);
                        } else {
                            questionsListener.onError(task.getException());
                        }
                    }
                });
    }

    public interface QuestionsListener {

        void onQuestionsReceived(@NonNull List<String> questions);

        void onError(@Nullable Throwable throwable);

    }

}
