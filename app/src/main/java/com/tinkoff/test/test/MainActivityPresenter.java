package com.tinkoff.test.test;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Patterns;

import com.tinkoff.test.test.data.Form;
import com.tinkoff.test.test.data.FormSender;
import com.tinkoff.test.test.data.QuestionsProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.tinkoff.test.test.MainContract.MainPresenter;
import static com.tinkoff.test.test.MainContract.MainView;

/**
 * @author Sergei Solodkov
 */
class MainActivityPresenter implements MainPresenter, QuestionsProvider.QuestionsListener {

    @NonNull
    private final FormSender formSender;
    @NonNull
    private final QuestionsProvider questionsProvider;
    private int currentQuestion = -1;
    @NonNull
    private final Map<String, String> answers = new HashMap<>();
    @NonNull
    private List<String> questions = new ArrayList<>();
    @Nullable
    private MainView mainView;

    MainActivityPresenter(@NonNull FormSender formSender,
                          @NonNull QuestionsProvider questionsProvider) {
        this.formSender = formSender;
        this.questionsProvider = questionsProvider;
    }

    @Override
    public void attach(@NonNull MainView mainView, int currentQuestion) {
        this.mainView = mainView;
        this.currentQuestion = currentQuestion;
        mainView.showLoading(true);
        questionsProvider.requestQuestions(this);
    }

    public void processAnswer(@Nullable String answer) {
        checkView(mainView);
        if (!isAnswerValid(answer)) {
            mainView.showMessage(R.string.error_input_answer);
            return;
        }
        answers.put(questions.get(currentQuestion), answer);
        if (currentQuestion < questions.size() - 1) {
            mainView.showQuestion(this.questions.get(++currentQuestion), currentQuestion, questions.size());
        } else {
            mainView.showPersonalDataForm();
        }
    }

    @Override
    public void processSubmit(@NonNull String firstName, @NonNull String lastName, @NonNull String email) {
        if (!isPersonalDataValid(firstName, lastName, email)){
            return;
        }
        checkView(mainView);
        mainView.showLoading(true);
        formSender.sendForm(new Form(answers, firstName, lastName, email), new FormSender.FormSenderListener() {
            @Override
            public void onFormSent() {
                if (mainView != null) {
                    mainView.showLoading(false);
                    mainView.showSuccessNotification();
                }
            }

            @Override
            public void onError(@NonNull String message) {
                if (mainView != null) {
                    mainView.showLoading(false);
                    mainView.showMessage(message);
                }
            }
        });
    }

    @Override
    public void release() {
        answers.clear();
        mainView = null;
    }

    @Override
    public void onQuestionsReceived(@NonNull List<String> questions) {
        if (currentQuestion > questions.size() - 1){
            currentQuestion = -1;
        }
        this.questions = new ArrayList<>(questions);
        if (mainView != null) {
            mainView.showLoading(false);
            if (currentQuestion + 1 < this.questions.size()) {
                mainView.showQuestion(this.questions.get(++currentQuestion), currentQuestion + 1, questions.size());
            }
        }
    }

    @Override
    public void onError(@Nullable Throwable throwable) {
        if (mainView != null) {
            mainView.showLoading(false);
            mainView.showMessage(throwable != null ? throwable.getMessage() : "Loading error");
        }
    }

    private void checkView(@Nullable MainView mainView) {
        if (mainView == null) {
            throw new IllegalStateException("Provide MainView");
        }
    }

    private boolean isAnswerValid(@Nullable String answer) {
        return !isStringEmpty(answer);
    }

    private boolean isPersonalDataValid(@NonNull String firstName, @NonNull String lastName, @NonNull String email){
        checkView(mainView);
        if (isStringEmpty(firstName)){
            mainView.showMessage(R.string.error_input_name);
            return false;
        }
        if (isStringEmpty(lastName)){
            mainView.showMessage(R.string.error_input_lastname);
            return false;
        }
        if (isStringEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            mainView.showMessage(R.string.error_input_email);
            return false;
        }
        return true;
    }

    private boolean isStringEmpty(@Nullable String s){
        return s == null || s.trim().isEmpty();
    }


}
