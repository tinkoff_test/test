package com.tinkoff.test.test.data;

import android.support.annotation.NonNull;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergei Solodkov
 */

class QuestionsParser {

    @NonNull
    List<String> parseQuestions(@NonNull String json){
        List<String> questions = new ArrayList<>();
        try {
            JSONArray questionsArray = new JSONObject(json).getJSONArray("questions");
            if (questionsArray.length() != 0) {
                for (int i = 0; i < questionsArray.length(); i++) {
                    questions.add(questionsArray.get(i).toString());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
        return questions;
    }

}
