package com.tinkoff.test.test;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * @author Sergei Solodkov
 */

interface MainContract {

    interface MainView {

        void showQuestion(@NonNull String question, int number, int count);

        void showPersonalDataForm();

        void showMessage(@NonNull String message);

        void showMessage(int messageResId);

        void showSuccessNotification();

        void showLoading(boolean isShown);
    }

    interface MainPresenter {

        void attach(@NonNull MainView mainView, int questionNumber);

        void processAnswer(@Nullable String answer);

        void processSubmit(@NonNull String firstName, @NonNull String lastName, @NonNull String email);

        void release();

    }

}
